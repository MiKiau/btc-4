# Paprasta svetaine pagrista Etherium kontraktais

VU Bloku technologiju kurso ketvirta uzduotis.

## Butinos programos

- ganache;
- truffle;

## Paleidimo instrukcijos

Patikrinta, kad veikia Linux OS, todel taip pat ja rekomenduojama naudoti.

- Nusikopijuoti failus is gitlab
- cd btc-4/back-end
- truffle migrate
- cd ../front-end
- npm install
- npm run build
- firefox index.html

## Naudojimosi instrukcijos

- Atlikus paleidimo instrukcijas tinklapyje pasirenkame produkto pavadinima ir suvedame jo kieki.
- Paspaudziame "Order", kad uzsakytume norima produkta.
- Atsiradus zinutei, kuri informuoja kiek reikia sumoketi, paspaudziame "Pay".
- Galiausiai, paspaudziame "Deliver".