// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.7.0 <0.9.0;

contract KebabInn {
    address payable public owner;

    struct Product {
        string name;
        uint price;
        uint totalAmount;

        bool init;
    }

    struct Buyer {
        address buyerAddr;
        string name;

        bool init;
    }

    struct Courier {
        address payable courierAddr;
        string name;
        
        bool init;
    }

    struct Delivery {
        Courier courier;
        uint price;
        uint date;
        bool delivered;

        bool init;
    }

    struct Order {
        address buyerAddr;
        string productName;
        uint quantity;
        uint price;
        uint safePay;
        Delivery delivery;

        bool init;
    }

    mapping(uint => Courier) couriers;
    uint courierSeq;

    mapping(address => Buyer) buyers;

    mapping(uint => Order) orders;
    uint orderSeq;

    mapping(uint => Product) products;
    uint productSeq;

    mapping(string => uint) productsByName;

    event ProductAdded(string _productName, uint _price, uint _amount);
    event CourierAdded(address _courierAddr, string _courierName);
    event BuyerRegistered(address _buyer, string _name);
    event OrderReceived(address _buyer, string _productName, uint _quantity, uint _orderno);
    event PricePayed(address _buyer, uint _orderno, uint _price, uint _now);
    event OrderDelivered(address _buyer, uint _orderno, uint _now);

    constructor() {
        owner = payable(msg.sender);
    }

    function addCourier(address _courierAddr, string memory _courierName) public {
        require(msg.sender == owner, "KebabInn:addCourier: This function is only available for owner.");

        couriers[courierSeq] = Courier(payable(_courierAddr), _courierName, true);
        courierSeq++;

        emit CourierAdded(_courierAddr, _courierName);
    }

    function addProduct(string memory _productName, uint _price, uint _amount) public returns (bool success) {
        require(msg.sender == owner, "KebabInn:addProduct: This function is only available for owner.");

        productsByName[_productName] = productSeq;
        products[productSeq] = Product(_productName, _price, _amount, true);
        productSeq++;

        emit ProductAdded(_productName, _price, _amount);
        return true;
    }

    function receiveOrder(string memory _productName, uint _quantity) payable public returns (bool success) {
        require(bytes(_productName).length > 0, "KebabInn:receiveOrder: Product name must be valid.");

        uint _price = products[productsByName[_productName]].price * _quantity;
        Delivery memory _delivery = Delivery(couriers[courierSeq - 1], _price / 10, 0, false, true);
        orders[orderSeq] = Order(msg.sender, _productName, _quantity, _price, 0, _delivery, true);

        emit OrderReceived(msg.sender, _productName, _quantity, orderSeq);
        return true;
    }

    function sendSafepay(uint _orderno) payable public {
        require(orders[_orderno].init, "KebabInn:sendSafepay: Order must be initialised.");
        require(buyers[msg.sender].init, "KebabInn:sendSafepay: This function is only available to initialised buyer.");
        require(orders[_orderno].price + orders[_orderno].delivery.price == msg.value);

        orders[_orderno].safePay = msg.value;
        emit PricePayed(msg.sender, _orderno, msg.value, block.timestamp);
    }

    function delivery(uint _orderno) payable public {
        require(orders[_orderno].init, "KebabInn:delivery: Order must be initialised.");

        Order storage _order = orders[_orderno];

        require(!_order.delivery.delivered, "KebabInn:delivery: Order must not be delivered.");
        require(_order.delivery.courier.courierAddr == msg.sender, "KebabInn:delivery: This function is available only for courier.");
        orders[_orderno].delivery.date = block.timestamp;
        
        emit OrderDelivered(_order.buyerAddr, _orderno, block.timestamp);
        owner.transfer(_order.safePay);
        _order.delivery.courier.courierAddr.transfer(_order.delivery.price);
    }
}