
const KEBAB_INN_ADDR = '0xf8Cb4BA197Fc1f04e91ae1b080C516Ed2B43D1BB';
const CONT_ADDR = '0x860fd4a9023836c2c88B72A88Cb01aC03f89C833';

const web3 = new Web3(Web3.givenProvider || "ws://localhost:8545");
window.web3 = web3;

const $ = (sel) => {
    return document.querySelector(sel);
}
  
/**
 * Gets account details
 */
async function getAccounts() {
    const res = [];
    const accounts = await web3.eth.getAccounts();
    for (const account of accounts) {
        const balanceWei = await web3.eth.getBalance(account);
        const balance = web3.utils.fromWei(balanceWei);
        res.push({
            account,
            balance,
        });
    }
    return res;
}

async function updateBalance() {
    const accounts = await getAccounts();
    $('#walletID').innerText = web3.utils.toWei(accounts[1].balance);
    $('#userAddressID').innerText = accounts[1].account;
    $('#statusID').innerText = "ready";
}

window.addEventListener('load', async () => {
    await updateBalance();
})

// Instantiate your contract
const myContractJson = require('./../back-end/build/contracts/SimplifiedKebabInn.json');
const myContract = new web3.eth.Contract(myContractJson.abi, CONT_ADDR);

// Bind to window
myContract.defaultAccount = KEBAB_INN_ADDR;
window.myContract = myContract;

// Data
products = {
    "Traditional-Kebab": 150,
    "Cool-Kebab": 250
};
order = -1;
payable = false;
deliverable = false;

$('#orderButtonID').addEventListener('click', async (event) => {
    const productName = document.getElementById("productNameID").value;

    if (productName != "(choose)" && order == -1) {
        event.disabled = true;

        const productAmount = $('#productQuantityID').value;
        const price = products[productName] * productAmount;

        const accounts = await web3.eth.getAccounts();
        console.log(`${accounts[1]} orders ${productAmount} of ${productName} and needs to pay ${price}`);
        await myContract.methods
            .receiveOrder(productName, productAmount, price)
            .send({from: accounts[1]});

        order = 0;
        payable = true;

        console.log(`${accounts[1]} ${productAmount} orders ${productName} and needs to pay ${price}`);
        $('#statusID').innerText = "please pay for the order";
        $('#priceID').innerText = price;

        event.disabled = false
    }
});

$('#payButtonID').addEventListener('click', async (event) => {
    if (order == 0 && payable) {
        event.srcElement.disabled = true;

        const accounts = await web3.eth.getAccounts();
        await myContract.methods
            .sendSafepay(order)
            .send({from: accounts[1]});

        payable = false;
        deliverable = true;

        console.log(`${accounts[1]}: Has payed for her items.`);
        $('#statusID').innerText = "products are deliverable";
        $('#priceID').innerText = 0;

        event.srcElement.disabled = false
    }
});

$('#deliverButtonID').addEventListener('click', async (event) => {
    if (deliverable) {
        event.srcElement.disabled = true;

        const accounts = await web3.eth.getAccounts();
        await myContract.methods
            .delivery(order)
            .send({from: accounts[1]});

        order = -1;
        deliverable = false;

        console.log(`${accounts[1]}: has received her items.`);
        $('#statusID').innerText = "ready";

        event.srcElement.disabled = false
    }
});