// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.7.0 <0.9.0;

contract SimplifiedKebabInn {
    address payable public owner;
    address payable public buyer;

    struct Order {
        address buyerAddr;
        string productName;
        uint quantity;
        uint price;
        uint safePay;

        bool init;
    }

    mapping(uint => Order) orders;
    uint orderSeq;

    event OrderReceived(address _buyer, string _productName, uint _quantity, uint _price, uint _orderno);
    event PricePayed(address _buyer, uint _orderno, uint _price, uint _now);
    event OrderDelivered(address _buyer, uint _orderno, uint _now);

    constructor() {
        owner = payable(msg.sender);
    }

    function receiveOrder(string memory _productName, uint _quantity, uint _price) payable public {
        require(bytes(_productName).length > 0, "KebabInn:receiveOrder: Product name must be valid.");

        buyer = payable(msg.sender);
        orders[orderSeq] = Order(msg.sender, _productName, _quantity, _price, 0, true);
        orderSeq++;

        emit OrderReceived(msg.sender, _productName, _quantity, _price, orderSeq - 1);
    }

    function sendSafepay(uint _orderno) payable public {
        require(orders[_orderno].init, "KebabInn:sendSafepay: Order must be initialised.");
        require(msg.sender == buyer, "KebabInn:sendSafepay: This function is only available to initialised buyer.");
        require(orders[_orderno].price == msg.value);

        orders[_orderno].safePay = msg.value;
        emit PricePayed(msg.sender, _orderno, msg.value, block.timestamp);
    }

    function delivery(uint _orderno) payable public {
        require(orders[_orderno].init, "KebabInn:delivery: Order must be initialised.");
        require(msg.sender == buyer, "KebabInn:delivery: This function is available only for buyer.");
        
        emit OrderDelivered(orders[_orderno].buyerAddr, _orderno, block.timestamp);
        owner.transfer(orders[_orderno].safePay);
    }
}